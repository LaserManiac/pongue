use std::sync::Arc;

use specs::prelude::*;

use rand::seq::SliceRandom;

use shrev::{EventChannel, ReaderId};

use crate::{
    graphics::Color,
    core::{GameStateConstructor, GameState, Command, NetworkEvent, TcpConnection, WORLD_WIDTH,
           WORLD_MIDDLE, WORLD_CENTER},
    ecs::{self, Position, Text, Animation, Velocity, NetworkSender, NetworkSystem, Sprite, Origin,
          Rotation, Tint, InputEvent, Key, NETWORK_SYSTEM},
    game_logic::{NetworkData, NetworkRole, Sprites, ErrorState, MenuState, Ball, BallSystem, Paddle,
                 PaddleControlSystem, PaddleMoveSystem, PADDLE_CONTROL_SYSTEM, PADDLE_MOVE_SYSTEM,
                 PADDLE_HEIGHT, PADDLE_POSITION_BOTTOM, PADDLE_POSITION_TOP, PADDLE_SELF_COLOR,
                 PADDLE_OPPONENT_COLOR, BALL_SYSTEM, BALL_HEIGHT, BALL_INITIAL_VELOCITY},
};


pub struct PlayingState {
    input: ReaderId<InputEvent>,
    network: ReaderId<NetworkEvent<NetworkData>>,
    dispatcher: Dispatcher<'static, 'static>,
    ball: Option<Entity>,
    text: Option<Entity>,
    playing: bool,
    self_ready: bool,
    other_ready: bool,
    score: (u32, u32),
}

impl GameStateConstructor for PlayingState {
    type Data = (
        NetworkRole,
        Arc<TcpConnection<NetworkData>>,
    );

    fn construct(world: &mut World, data: Self::Data) -> Self {
        let (role, connection) = data;

        world.add_resource(role);
        world.register::<Paddle>();
        world.register::<Ball>();

        let mut dispatcher = DispatcherBuilder::new()
            .with(NetworkSystem::new(connection), NETWORK_SYSTEM, &[])
            .with(BallSystem::new(world), BALL_SYSTEM, &[NETWORK_SYSTEM])
            .with(PaddleControlSystem::new(world), PADDLE_CONTROL_SYSTEM, &[NETWORK_SYSTEM])
            .with(PaddleMoveSystem, PADDLE_MOVE_SYSTEM, &[PADDLE_CONTROL_SYSTEM])
            .build();
        dispatcher.setup(&mut world.res);

        let spr_paddle = Read::<Sprites>::fetch(&world.res).paddle;
        world.create_entity()
            .with(Paddle::remote())
            .with(Velocity(0.0, 0.0))
            .with(Position(WORLD_WIDTH * 0.5, PADDLE_POSITION_TOP))
            .with(Sprite(spr_paddle))
            .with(Origin(8.0, 2.0))
            .with(Rotation(0.0))
            .with(Tint(PADDLE_OPPONENT_COLOR))
            .build();
        world.create_entity()
            .with(Paddle::local())
            .with(Velocity(0.0, 0.0))
            .with(Position(WORLD_WIDTH * 0.5, PADDLE_POSITION_BOTTOM))
            .with(Sprite(spr_paddle))
            .with(Origin(8.0, 2.0))
            .with(Rotation(180.0))
            .with(Tint(PADDLE_SELF_COLOR))
            .build();

        let text = ecs::spawn_text(
            world,
            "PRESS\nSPACE TO\nSTART",
            WORLD_MIDDLE,
            WORLD_CENTER,
            Color::white(),
            1.0,
            None,
        ).into();

        let network = Write::<EventChannel<_>>::fetch(&world.res).register_reader();
        let input = Write::<EventChannel<_>>::fetch(&world.res).register_reader();

        Self {
            input,
            network,
            dispatcher,
            ball: None,
            text,
            playing: false,
            self_ready: false,
            other_ready: false,
            score: (0, 0),
        }
    }
}

impl GameState for PlayingState {
    fn update(&mut self, world: &mut World) -> Command {
        self.dispatcher.dispatch(&world.res);

        let role = ReadExpect::<NetworkRole>::fetch(&world.res);
        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.network) {
            match event {
                NetworkEvent::Disconnected =>
                    return Command::to::<ErrorState>("OPPONENT\nDISCONNECTED!".into()),
                NetworkEvent::Data(NetworkData::ClientReady) if role.is_host() =>
                    self.set_other_ready(world),
                NetworkEvent::Data(NetworkData::BallSpawn(angle)) if role.is_client() =>
                    self.spawn_ball(world, *angle),
                NetworkEvent::Data(NetworkData::UpdateScore(h, c)) if role.is_client() =>
                    self.on_scored(world, (*h, *c)),
                _ => (),
            }
        }

        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.input) {
            match event {
                InputEvent::Close => return Command::Exit,
                InputEvent::Key(Key::Escape, true) => return Command::to::<MenuState>(()),
                InputEvent::Key(Key::Space, true) => self.set_self_ready(world),
                _ => (),
            }
        }

        if role.is_host() {
            if let (true, Some(ball)) = (self.playing, self.ball) {
                let y = ReadStorage::<Position>::fetch(&world.res)
                    .get(ball)
                    .unwrap().1;
                let top = PADDLE_POSITION_TOP + (PADDLE_HEIGHT + BALL_HEIGHT) * 0.5;
                let bottom = PADDLE_POSITION_BOTTOM - (PADDLE_HEIGHT + BALL_HEIGHT) * 0.5;
                if y < top {
                    self.on_scored(world, (self.score.0 + 1, self.score.1));
                } else if y > bottom {
                    self.on_scored(world, (self.score.0, self.score.1 + 1));
                }
            }
        }

        Command::Continue
    }

    fn end(self: Box<Self>, world: &mut World) {
        self.dispatcher.dispose(&mut world.res);
        world.delete_all();
    }

    fn name(&self) -> &'static str { "playing_state" }
}

impl PlayingState {
    fn set_self_ready(&mut self, world: &World) {
        if self.self_ready {
            return;
        }

        if let Some(text) = self.text {
            let _ = WriteStorage::<Text>::fetch(&world.res)
                .get_mut(text)
                .unwrap()
                .0 = String::from("WAITING\nFOR\nOPPONENT");
            let _ = WriteStorage::fetch(&world.res)
                .insert(text, Animation::hover_slow());
        }

        self.self_ready = true;

        let role = *ReadExpect::<NetworkRole>::fetch(&world.res);
        if role.is_client() {
            ReadExpect::<NetworkSender<_>>::fetch(&world.res).send(NetworkData::ClientReady);
        } else if role.is_host() {
            self.host_attempt_start_game(world);
        }
    }

    fn set_other_ready(&mut self, world: &World) {
        if self.other_ready {
            return;
        }

        self.other_ready = true;
        if ReadExpect::<NetworkRole>::fetch(&world.res).is_host() {
            self.host_attempt_start_game(world);
        }
    }

    fn host_attempt_start_game(&mut self, world: &World) {
        if self.self_ready && self.other_ready {
            self.playing = true;
            let angle = Self::pick_ball_angle();
            self.spawn_ball(world, angle);
            WriteExpect::<NetworkSender<_>>::fetch(&world.res)
                .send(NetworkData::BallSpawn(angle));
        }
    }

    fn spawn_ball(&mut self, world: &World, angle: f32) {
        if let Some(ball) = self.ball {
            let _ = Entities::fetch(&world.res).delete(ball);
            self.ball = None;
        }

        if let Some(text) = self.text {
            let _ = Entities::fetch(&world.res).delete(text);
            self.text = None;
        }

        let mut velocity = Velocity::new_direction(angle, BALL_INITIAL_VELOCITY);
        if ReadExpect::<NetworkRole>::fetch(&world.res).is_client() {
            velocity.1 = -velocity.1;
        }

        let spr_ball = Read::<Sprites>::fetch(&world.res).ball;
        let ox = spr_ball.width() as f32 * 0.5;
        let oy = spr_ball.height() as f32 * 0.5;

        self.ball = world.create_entity_unchecked()
            .with(Ball::new())
            .with(velocity)
            .with(Position(WORLD_MIDDLE, WORLD_CENTER))
            .with(Sprite(spr_ball))
            .with(Origin(ox, oy))
            .build()
            .into();
    }

    fn on_scored(&mut self, world: &World, (host, client): (u32, u32)) {
        if let Some(ball) = self.ball {
            WriteStorage::<Ball>::fetch(&world.res)
                .get_mut(ball)
                .unwrap()
                .disable_collision();
        }

        let (old_host, _) = self.score;
        let host_scored = host > old_host;
        self.score = (host, client);

        let role = *ReadExpect::<NetworkRole>::fetch(&world.res);
        let is_winner = role.is_host() && host_scored || role.is_client() && !host_scored;
        let (first, second) = if role.is_host() { (host, client) } else { (client, host) };
        let (text, color) = if is_winner {
            (format!("YOU\nSCORED\n{}-{}", first, second), PADDLE_SELF_COLOR)
        } else {
            (format!("OPPONENT\nSCORED\n{}-{}", first, second), PADDLE_OPPONENT_COLOR)
        };
        self.text = ecs::spawn_text(
            world,
            text,
            WORLD_MIDDLE,
            WORLD_CENTER,
            color,
            1.0,
            Animation::drop(),
        ).into();

        self.playing = false;
        self.self_ready = false;
        self.other_ready = false;

        WriteExpect::<NetworkSender<_>>::fetch(&world.res)
            .send(NetworkData::UpdateScore(host, client));
    }

    fn pick_ball_angle() -> f32 {
        let angles = [60.0, 75.0, 105.0, 120.0, 240.0, 255.0, 285.0, 300.0];
        SliceRandom::choose(angles.as_ref(), &mut rand::thread_rng()).cloned().unwrap()
    }
}
