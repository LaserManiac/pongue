use std::{sync::Arc, net::IpAddr};

use specs::prelude::*;

use shrev::{EventChannel, ReaderId};

use crate::{
    graphics::Color,
    core::{GameStateConstructor, GameState, Command, NetworkEvent, TcpConnection, WORLD_MIDDLE,
           WORLD_CENTER},
    ecs::{self, Animation, NetworkSystem, NetworkReceiver, InputEvent, Key, NETWORK_SYSTEM},
    game_logic::{NetworkData, NetworkRole, ErrorState, JoinState, PlayingState},
};


pub struct ConnectingState {
    input: ReaderId<InputEvent>,
    network: ReaderId<NetworkEvent<NetworkData>>,
    dispatcher: Dispatcher<'static, 'static>,
    connection: Arc<TcpConnection<NetworkData>>,
}

impl GameStateConstructor for ConnectingState {
    type Data = IpAddr;

    fn construct(world: &mut World, data: Self::Data) -> Self {
        let connection = Arc::new(TcpConnection::connect(data));

        let mut dispatcher = DispatcherBuilder::new()
            .with(NetworkSystem::new(Arc::clone(&connection)), NETWORK_SYSTEM, &[])
            .build();
        dispatcher.setup(&mut world.res);

        let mut receiver = NetworkReceiver::new();
        let network = receiver.register_reader();
        world.add_resource(receiver);

        let input = Write::<EventChannel<_>>::fetch(&world.res).register_reader();

        ecs::spawn_text(
            world,
            "JOINING",
            WORLD_MIDDLE,
            WORLD_CENTER,
            Color::white(),
            1.0,
            Animation::hover_slow(),
        );

        Self { input, network, dispatcher, connection }
    }
}

impl GameState for ConnectingState {
    fn update(&mut self, world: &mut World) -> Command {
        self.dispatcher.dispatch(&world.res);

        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.network) {
            match event {
                NetworkEvent::Connected =>
                    return Command::to::<PlayingState>((
                        NetworkRole::Client,
                        Arc::clone(&self.connection),
                    )),
                NetworkEvent::Error =>
                    return Command::to::<ErrorState>("COULD NOT\nCONNECT!".into()),
                _ => (),
            }
        }

        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.input) {
            match event {
                InputEvent::Close => return Command::Exit,
                InputEvent::Key(Key::Escape, true) => return Command::to::<JoinState>(()),
                _ => (),
            };
        }

        Command::Continue
    }

    fn end(self: Box<Self>, world: &mut World) {
        self.dispatcher.dispose(&mut world.res);
        world.delete_all();
    }

    fn name(&self) -> &'static str { "connecting_state" }
}