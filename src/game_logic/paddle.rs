use specs::prelude::*;

use shrev::{EventChannel, ReaderId};

use crate::{
    graphics::Color,
    core::{DeltaTime, NetworkEvent, WORLD_WIDTH, WORLD_HEIGHT},
    ecs::{Position, Velocity, NetworkSender, NetworkReceiver, InputEvent, Key},
    game_logic::{NetworkData, NetworkRole, BallColor},
};


enum PaddleKind {
    Local,
    Remote,
}


#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Paddle(PaddleKind, i8);

impl Paddle {
    pub fn local() -> Self {
        Self(PaddleKind::Local, 0)
    }

    pub fn remote() -> Self {
        Self(PaddleKind::Remote, 0)
    }

    pub fn ball_color(&self, role: NetworkRole) -> BallColor {
        match (&self.0, role) {
            (PaddleKind::Local, NetworkRole::Host) => BallColor::Host,
            (PaddleKind::Local, NetworkRole::Client) => BallColor::Client,
            (PaddleKind::Remote, NetworkRole::Host) => BallColor::Client,
            (PaddleKind::Remote, NetworkRole::Client) => BallColor::Host,
        }
    }
}


pub const PADDLE_WIDTH: f32 = 16.0;
pub const PADDLE_HEIGHT: f32 = 4.0;
pub const PADDLE_POSITION_TOP: f32 = WORLD_HEIGHT * 0.05;
pub const PADDLE_POSITION_BOTTOM: f32 = WORLD_HEIGHT * 0.95;
pub const PADDLE_SELF_COLOR: Color = Color::rgb(0, 63, 255);
pub const PADDLE_OPPONENT_COLOR: Color = Color::rgb(255, 0, 0);
const PADDLE_MAX_VELOCITY: f32 = 50.0;
const PADDLE_ACCELERATION: f32 = 200.0;


pub const PADDLE_CONTROL_SYSTEM: &str = "paddle_control";

pub struct PaddleControlSystem {
    input: ReaderId<InputEvent>,
    network: ReaderId<NetworkEvent<NetworkData>>,
}

impl PaddleControlSystem {
    pub fn new(world: &World) -> Self {
        let input = Write::<EventChannel<_>>::fetch(&world.res).register_reader();
        let network = Write::<EventChannel<_>>::fetch(&world.res).register_reader();
        Self { input, network }
    }
}

impl<'a> System<'a> for PaddleControlSystem {
    type SystemData = (
        ReadExpect<'a, EventChannel<InputEvent>>,
        ReadExpect<'a, NetworkRole>,
        ReadExpect<'a, NetworkReceiver<NetworkData>>,
        ReadExpect<'a, NetworkSender<NetworkData>>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Paddle>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (input, role, net_recv, net_send, mut positions, mut paddles) = data;

        let mut dir = None;
        for event in input.read(&mut self.input) {
            match event {
                InputEvent::Key(Key::Left, true) => dir = Some(-1),
                InputEvent::Key(Key::Left, false) => dir = Some(0),
                InputEvent::Key(Key::Right, true) => dir = Some(1),
                InputEvent::Key(Key::Right, false) => dir = Some(0),
                _ => (),
            }
        }

        if let Some(dir) = dir {
            let joined = (&mut paddles, &positions).join();
            for (Paddle(kind, ref mut direction), Position(position, _)) in joined {
                if let PaddleKind::Local = kind {
                    net_send.send(NetworkData::PaddleDirectionSync(dir));
                    if role.is_host() {
                        net_send.send(NetworkData::PaddleHostPositionSync(*position));
                    }
                    *direction = dir;
                }
            }
        }

        for event in net_recv.read(&mut self.network) {
            match event {
                NetworkEvent::Data(NetworkData::PaddleDirectionSync(dir)) => {
                    for Paddle(kind, direction) in (&mut paddles).join() {
                        if let PaddleKind::Remote = kind {
                            *direction = *dir;
                        }
                    }
                }
                NetworkEvent::Data(NetworkData::PaddleHostPositionSync(pos))
                if role.is_client() => {
                    let joined = (&mut paddles, &mut positions).join();
                    for (Paddle(kind, _), Position(position, _)) in joined {
                        if let PaddleKind::Remote = kind {
                            *position = *pos;
                        }
                    }
                }
                NetworkEvent::Data(NetworkData::PaddleClientPositionSync(pos))
                if role.is_client() => {
                    let joined = (&mut paddles, &mut positions).join();
                    for (Paddle(kind, _), Position(position, _)) in joined {
                        if let PaddleKind::Local = kind {
                            *position = *pos;
                        }
                    }
                }
                _ => (),
            }
        }
    }
}


pub const PADDLE_MOVE_SYSTEM: &str = "paddle_move";

pub struct PaddleMoveSystem;

impl<'a> System<'a> for PaddleMoveSystem {
    type SystemData = (
        Read<'a, DeltaTime>,
        ReadStorage<'a, Paddle>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (delta, paddles, mut positions, mut velocities) = data;
        let acceleration = PADDLE_ACCELERATION * delta.as_f32();
        let world_start = PADDLE_WIDTH * 0.5;
        let world_end = WORLD_WIDTH - PADDLE_WIDTH * 0.5;
        let joined = (&paddles, &mut velocities, &mut positions).join();
        for (paddle, mut velocity, mut position) in joined {
            let direction = paddle.1;
            let vel_x = velocity.0;
            let pos_x = position.0;
            velocity.0 = if pos_x < world_start {
                position.0 = world_start;
                0.0
            } else if pos_x > world_end {
                position.0 = world_end;
                0.0
            } else if direction > 0 {
                f32::min(vel_x + acceleration, PADDLE_MAX_VELOCITY)
            } else if direction < 0 {
                f32::max(vel_x - acceleration, -PADDLE_MAX_VELOCITY)
            } else if vel_x > 0.0 {
                f32::max(vel_x - acceleration, 0.0)
            } else if vel_x < 0.0 {
                f32::min(vel_x + acceleration, 0.0)
            } else {
                vel_x
            };
        }
    }
}
