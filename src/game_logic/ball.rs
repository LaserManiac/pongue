use specs::prelude::*;

use shrev::ReaderId;

use crate::{
    graphics::Color,
    core::{NetworkEvent, DeltaTime, WORLD_WIDTH, WORLD_HEIGHT},
    ecs::{Position, Tint, Velocity, NetworkSender, NetworkReceiver},
    game_logic::{NetworkRole, NetworkData, Paddle, PADDLE_WIDTH, PADDLE_HEIGHT, PADDLE_SELF_COLOR,
                 PADDLE_OPPONENT_COLOR},
};


pub const BALL_WIDTH: f32 = 3.0;
pub const BALL_HEIGHT: f32 = 3.0;
pub const BALL_INITIAL_VELOCITY: f32 = 30.0;
pub const BALL_MAX_VELOCITY: f32 = 75.0;
pub const BALL_ACCELERATION: f32 = 1.0;


#[derive(Component, Default)]
#[storage(DenseVecStorage)]
pub struct Ball {
    has_collision: bool,
}

impl Ball {
    pub fn new() -> Self {
        Self { has_collision: true }
    }

    pub fn disable_collision(&mut self) {
        self.has_collision = false;
    }
}


#[derive(Copy, Clone, Debug)]
pub enum BallColor {
    Host,
    Client,
    Neutral,
}

impl BallColor {
    pub fn color(&self, role: NetworkRole) -> Color {
        match (self, role) {
            (BallColor::Neutral, _) => Color::white(),
            (BallColor::Host, NetworkRole::Host)
            | (BallColor::Client, NetworkRole::Client) => PADDLE_SELF_COLOR,
            _ => PADDLE_OPPONENT_COLOR,
        }
    }
}

impl Into<u8> for BallColor {
    fn into(self) -> u8 {
        match self {
            BallColor::Host => 0,
            BallColor::Client => 1,
            BallColor::Neutral => 2,
        }
    }
}

impl From<u8> for BallColor {
    fn from(byte: u8) -> Self {
        match byte {
            0 => BallColor::Host,
            1 => BallColor::Client,
            _ => BallColor::Neutral,
        }
    }
}


pub const BALL_SYSTEM: &str = "ball";

pub struct BallSystem {
    reader: ReaderId<NetworkEvent<NetworkData>>,
}

type BallSystemColliderJoin<'a> = (&'a Paddle, &'a Position, &'a Velocity);

impl BallSystem {
    pub fn new(world: &World) -> Self {
        let reader = Write::<NetworkReceiver<NetworkData>>::fetch(&world.res)
            .register_reader();
        Self { reader }
    }

    fn collides<'a>(x: f32,
                    y: f32,
                    delta: f32,
                    joined: impl Iterator<Item=BallSystemColliderJoin<'a>>,
                    role: NetworkRole)
                    -> Option<BallColor> {
        if Self::collides_with_wall(x) {
            Some(BallColor::Neutral)
        } else {
            Self::collides_with_paddles(x, y, delta, joined, role)
        }
    }

    fn collides_with_paddles<'a>(x: f32,
                                 y: f32,
                                 delta: f32,
                                 joined: impl Iterator<Item=BallSystemColliderJoin<'a>>,
                                 role: NetworkRole)
                                 -> Option<BallColor> {
        for (paddle, position, velocity) in joined {
            let Position(px, py) = *position;
            let Velocity(vx, _) = *velocity;
            let npx = px + vx * delta;
            let phw = PADDLE_WIDTH * 0.5;
            let phh = PADDLE_HEIGHT * 0.5;
            let bhw = BALL_WIDTH * 0.5;
            let bhh = BALL_HEIGHT * 0.5;
            let hor = phw + bhw;
            let ver = phh + bhh;
            if y > py - ver && y < py + ver {
                if x > npx - hor && x < npx + hor {
                    return Some(paddle.ball_color(role));
                } else if x > px - hor && x < px + hor {
                    return Some(paddle.ball_color(role));
                }
            }
        }
        None
    }

    fn collides_with_wall(x: f32) -> bool {
        x < BALL_WIDTH / 2.0 || x > WORLD_WIDTH - BALL_WIDTH / 2.0
    }
}

impl<'a> System<'a> for BallSystem {
    type SystemData = (
        Entities<'a>,
        Read<'a, DeltaTime>,
        ReadExpect<'a, NetworkRole>,
        ReadExpect<'a, NetworkReceiver<NetworkData>>,
        ReadExpect<'a, NetworkSender<NetworkData>>,
        ReadStorage<'a, Paddle>,
        WriteStorage<'a, Ball>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Velocity>,
        WriteStorage<'a, Tint>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            entities,
            delta,
            role,
            receiver,
            sender,
            paddles,
            mut balls,
            mut positions,
            mut velocities,
            mut tints,
        ) = data;

        for (entity, ball, position) in (&entities, &mut balls, &positions).join() {
            let delta = delta.as_f32();
            let Velocity(vx, vy) = velocities.get(entity)
                .unwrap()
                .accelerate_towards(BALL_MAX_VELOCITY, BALL_ACCELERATION * delta);
            let Position(x, y) = *position;
            let nx = x + vx * delta;
            let ny = y + vy * delta;

            if ball.has_collision {
                let mut nvx = vx;
                let mut nvy = vy;
                let mut bounce = None;
                let data = (&paddles, &positions, &velocities);
                if let Some(color) = Self::collides(nx, y, delta, data.join(), *role) {
                    nvx = -nvx;
                    bounce = Some(color);
                    if let Some(color) = Self::collides(x, ny, delta, data.join(), *role) {
                        nvy = -nvy;
                        bounce = Some(color);
                    }
                } else if let Some(color) = Self::collides(nx, ny, delta, data.join(), *role) {
                    nvy = -nvy;
                    bounce = Some(color);
                }

                *velocities.get_mut(entity).unwrap() = Velocity(nvx, nvy);
                if let Some(color) = bounce {
                    let _ = tints.insert(entity, Tint(color.color(*role)));
                    if role.is_host() {
                        sender.send(NetworkData::BallSync(x, y, nvx, nvy, color));
                    }
                }
            }
        }

        for event in receiver.read(&mut self.reader) {
            if let NetworkEvent::Data(NetworkData::BallSync(x, y, vx, vy, c)) = event {
                let joined = (&entities, &balls, &mut positions, &mut velocities).join();
                for (entity, _, position, velocity) in joined {
                    let y = if role.is_client() { WORLD_HEIGHT - *y } else { *y };
                    let vy = if role.is_client() { -*vy } else { *vy };
                    *position = Position(*x, y);
                    *velocity = Velocity(*vx, vy);
                    let _ = tints.insert(entity, Tint(c.color(*role)));
                }
            }
        }
    }
}
