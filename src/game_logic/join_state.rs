use std::{str::FromStr, net::IpAddr};

use specs::prelude::*;

use shrev::{EventChannel, ReaderId};

use crate::{
    graphics::Color,
    core::{GameStateConstructor, GameState, Command, WORLD_WIDTH, WORLD_HEIGHT},
    ecs::{self, Text, InputEvent, Key},
    game_logic::{ErrorState, MenuState, ConnectingState},
};


pub struct JoinState {
    input: ReaderId<InputEvent>,
    ip: String,
    ip_text: Entity,
}

impl JoinState {
    fn update_ip_text(&mut self, world: &World) {
        WriteStorage::<Text>::fetch(&world.res)
            .get_mut(self.ip_text)
            .unwrap()
            .0 = self.ip.clone();
    }
}

impl GameStateConstructor for JoinState {
    type Data = ();

    fn construct(world: &mut World, _: Self::Data) -> Self {
        ecs::spawn_text(
            world,
            "ENTER IP",
            WORLD_WIDTH * 0.5,
            WORLD_HEIGHT * 0.4,
            Color::white(),
            1.0, None,
        );

        let ip_text = ecs::spawn_text(
            world,
            String::new(),
            WORLD_WIDTH * 0.5,
            WORLD_HEIGHT * 0.6,
            Color::white(),
            0.6,
            None,
        );

        let input = Write::<EventChannel<_>>::fetch(&world.res).register_reader();

        Self {
            input,
            ip: String::new(),
            ip_text,
        }
    }
}

impl GameState for JoinState {
    fn update(&mut self, world: &mut World) -> Command {
        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.input) {
            match event {
                InputEvent::Close => return Command::Exit,
                InputEvent::Key(Key::Escape, true) => return Command::to::<MenuState>(()),
                InputEvent::Key(Key::Space, true) => return match IpAddr::from_str(&self.ip) {
                    Ok(ip) => Command::to::<ConnectingState>(ip),
                    Err(_) => Command::to::<ErrorState>("INVALID ADDRESS!".into()),
                },
                InputEvent::Char(chr) => if self.ip.len() < 15 && "0123456789.".contains(*chr) {
                    self.ip.push(*chr);
                    self.update_ip_text(world);
                }
                InputEvent::Key(Key::Back, true) => {
                    self.ip.pop();
                    self.update_ip_text(world);
                }
                _ => (),
            }
        }

        Command::Continue
    }

    fn end(self: Box<Self>, world: &mut World) {
        world.delete_all();
    }

    fn name(&self) -> &'static str { "join_state" }
}
