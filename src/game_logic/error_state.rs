use specs::prelude::*;

use shrev::{EventChannel, ReaderId};

use crate::{
    graphics::Color, game_logic::MenuState,
    ecs::{self, InputEvent, Key},
    core::{GameStateConstructor, GameState, Command, WORLD_MIDDLE, WORLD_CENTER},
};


pub struct ErrorState {
    input: ReaderId<InputEvent>,
}

impl GameStateConstructor for ErrorState {
    type Data = String;

    fn construct(world: &mut World, data: Self::Data) -> Self {
        ecs::spawn_text(world, data, WORLD_MIDDLE, WORLD_CENTER, Color::white(), 0.6, None);
        let input = Write::<EventChannel<_>>::fetch(&world.res).register_reader();
        Self { input }
    }
}

impl GameState for ErrorState {
    fn update(&mut self, world: &mut World) -> Command {
        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.input) {
            match event {
                InputEvent::Close => return Command::Exit,
                InputEvent::Key(Key::Escape, true) | InputEvent::Key(Key::Space, true) =>
                    return Command::to::<MenuState>(()),
                _ => ()
            }
        }
        Command::Continue
    }

    fn end(self: Box<Self>, world: &mut World) { world.delete_all(); }

    fn name(&self) -> &'static str { "error_state" }
}