use specs::prelude::*;

use crate::{
    ecs::Background,
    core::{log, GameStateConstructor, GameState, Command},
    graphics::{Color, TextureRegion, Font},
    game_logic::{Sprites, MenuState},
};


pub struct InitState;

impl GameStateConstructor for InitState {
    type Data = ();

    fn construct(world: &mut World, _: Self::Data) -> Self {
        let sprites = Sprites::default();
        let font = {
            let chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ?1234567890.!-<>";
            let glyph_size = [5, 5];
            let region = TextureRegion::new(16, 0, 45, 25);
            let default_glyph = Some([40, 10]);
            Font::new(chars, glyph_size, region, default_glyph)
        };
        let background = Background::new(sprites.solid_color, Color::black());

        world.add_resource(sprites);
        world.add_resource(font);
        world.add_resource(background);

        log::info("Initialization state constructed!");

        Self
    }
}

impl GameState for InitState {
    fn update(&mut self, _: &mut World) -> Command {
        Command::to::<MenuState>(())
    }

    fn end(self: Box<Self>, _: &mut World) {}

    fn name(&self) -> &'static str { "init_state" }
}
