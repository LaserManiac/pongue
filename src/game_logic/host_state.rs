use std::sync::Arc;

use specs::prelude::*;

use shrev::{EventChannel, ReaderId};

use crate::{
    graphics::Color,
    core::{GameStateConstructor, GameState, Command, TcpConnection, NetworkEvent, WORLD_MIDDLE,
           WORLD_HEIGHT},
    ecs::{self, Animation, InputEvent, Key, NetworkReceiver, NetworkSystem, NETWORK_SYSTEM},
    game_logic::{NetworkData, NetworkRole, ErrorState, MenuState, PlayingState},
};


fn fetch_host_ip() -> Option<String> {
    reqwest::get("https://ipecho.net/plain").ok()?.text().ok()
}


pub struct HostState {
    input: ReaderId<InputEvent>,
    dispatcher: Dispatcher<'static, 'static>,
    connection: Arc<TcpConnection<NetworkData>>,
    network: ReaderId<NetworkEvent<NetworkData>>,
}

impl GameStateConstructor for HostState {
    type Data = ();

    fn construct(world: &mut World, _: Self::Data) -> Self {
        let connection = Arc::new(TcpConnection::host());

        let mut dispatcher = DispatcherBuilder::new()
            .with(NetworkSystem::new(Arc::clone(&connection)), NETWORK_SYSTEM, &[])
            .build();
        dispatcher.setup(&mut world.res);

        let mut receiver = NetworkReceiver::new();
        let network = receiver.register_reader();
        world.add_resource(receiver);

        let input = Write::<EventChannel<_>>::fetch(&world.res).register_reader();

        ecs::spawn_text(
            world,
            "WAITING\nFOR\nOPPONENT",
            WORLD_MIDDLE,
            WORLD_HEIGHT * 0.35,
            Color::white(),
            1.0,
            Animation::hover_slow(),
        );

        let ip_text = fetch_host_ip()
            .map(|ip| format!("HOSTING ON\n{}", ip))
            .unwrap_or_else(|| String::from("COULD NOT\nGET HOST IP"));
        ecs::spawn_text(
            world,
            ip_text,
            WORLD_MIDDLE,
            WORLD_HEIGHT * 0.75,
            Color::white(),
            0.6,
            None,
        );

        Self { input, dispatcher, network, connection }
    }
}

impl GameState for HostState {
    fn update(&mut self, world: &mut World) -> Command {
        self.dispatcher.dispatch(&world.res);

        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.network) {
            match event {
                NetworkEvent::Connected =>
                    return Command::to::<PlayingState>((
                        NetworkRole::Host,
                        Arc::clone(&self.connection),
                    )),
                NetworkEvent::Error =>
                    return Command::to::<ErrorState>("COULD NOT\nSTART SERVER!".into()),
                _ => (),
            }
        }

        for event in Read::<EventChannel<_>>::fetch(&world.res).read(&mut self.input) {
            match event {
                InputEvent::Close => return Command::Exit,
                InputEvent::Key(Key::Escape, true) => return Command::to::<MenuState>(()),
                _ => (),
            }
        }

        Command::Continue
    }

    fn end(self: Box<Self>, world: &mut World) {
        self.dispatcher.dispose(&mut world.res);
        world.delete_all();
    }

    fn name(&self) -> &'static str { "host_state" }
}
