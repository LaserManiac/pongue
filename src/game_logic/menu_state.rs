use specs::prelude::*;

use shrev::{EventChannel, ReaderId};

use crate::{
    graphics::Color,
    core::{GameStateConstructor, GameState, Command, WORLD_MIDDLE, WORLD_HEIGHT},
    ecs::{self, Animation, Rotation, Scale, Tint, InputEvent, Key},
    game_logic::{HostState, JoinState},
};


const BUTTON_COLOR: Color = Color::white();
const BUTTON_SELECTED_COLOR: Color = Color::light_gray();


pub struct MenuState {
    input: ReaderId<InputEvent>,
    buttons: Vec<Entity>,
    selected: usize,
}

impl GameStateConstructor for MenuState {
    type Data = ();

    fn construct(world: &mut World, _: Self::Data) -> Self {
        ecs::spawn_text(
            world,
            "PONGUE",
            WORLD_MIDDLE,
            WORLD_HEIGHT * 0.25,
            Color::white(),
            1.75,
            None,
        );

        let input = Write::<EventChannel<_>>::fetch(&world.res).register_reader();

        let mut this = Self {
            input,
            buttons: Vec::new(),
            selected: 0,
        };
        this.spawn_buttons(world, &["HOST", "JOIN", "EXIT"]);
        this
    }
}

impl GameState for MenuState {
    fn update(&mut self, world: &mut World) -> Command {
        for event in Read::<EventChannel<InputEvent>>::fetch(&world.res).read(&mut self.input) {
            match event {
                InputEvent::Close | InputEvent::Key(Key::Escape, true) => return Command::Exit,
                InputEvent::Key(Key::Up, true) => self.select_up(world),
                InputEvent::Key(Key::Down, true) => self.select_down(world),
                InputEvent::Key(Key::Space, true) => return match self.selected {
                    0 => Command::to::<HostState>(()),
                    1 => Command::to::<JoinState>(()),
                    _ => Command::Exit,
                },
                _ => (),
            }
        }
        Command::Continue
    }

    fn end(self: Box<Self>, world: &mut World) {
        world.delete_all();
    }

    fn name(&self) -> &'static str { "menu_state" }
}

impl MenuState {
    fn spawn_buttons(&mut self, world: &mut World, buttons: &[&str]) {
        let height = 10.0;
        let total_height = buttons.len() as f32 * height;
        let x = WORLD_MIDDLE;
        let y = WORLD_HEIGHT * 0.6 - total_height * 0.5;
        for (index, text) in buttons.iter().enumerate() {
            let y = y + height * index as f32;
            let button = ecs::spawn_text(world, *text, x, y, BUTTON_COLOR, 1.0, None);
            self.buttons.push(button);
        }
        self.move_selection(world, 0);
    }

    fn move_selection(&mut self, world: &World, new: usize) {
        let current = self.buttons[self.selected];
        let _ = WriteStorage::<Animation>::fetch(&world.res).remove(current);
        let _ = WriteStorage::fetch(&world.res).insert(current, Rotation(0.0));
        let _ = WriteStorage::fetch(&world.res).insert(current, Scale(1.0, 1.0));
        let _ = WriteStorage::fetch(&world.res).insert(current, Tint(BUTTON_COLOR));

        self.selected = new;
        let current = self.buttons[new];
        let _ = WriteStorage::fetch(&world.res).insert(current, Animation::hover_fast());
        let _ = WriteStorage::fetch(&world.res).insert(current, Tint(BUTTON_SELECTED_COLOR));
    }

    fn select_up(&mut self, world: &World) {
        let new = if self.selected == 0 {
            self.buttons.len() - 1
        } else {
            self.selected - 1
        };
        self.move_selection(world, new);
    }

    fn select_down(&mut self, world: &World) {
        let new = if self.selected == self.buttons.len() - 1 {
            0
        } else {
            self.selected + 1
        };
        self.move_selection(world, new);
    }
}
