use crate::graphics::TextureRegion;

#[allow(unused)]
pub struct Sprites {
    pub solid_color: TextureRegion,
    pub ball: TextureRegion,
    pub paddle: TextureRegion,
}

impl Default for Sprites {
    fn default() -> Self {
        Self {
            solid_color: TextureRegion::new(15, 0, 1, 1),
            ball: TextureRegion::new(0, 0, 4, 4),
            paddle: TextureRegion::new(0, 4, 16, 4),
        }
    }
}
