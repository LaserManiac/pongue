use byteorder::{ReadBytesExt, WriteBytesExt, NetworkEndian};

use crate::{
    game_logic::BallColor,
    core::{log, NetworkPacket},
};


#[derive(Copy, Clone)]
pub enum NetworkRole {
    Host,
    Client,
}

impl NetworkRole {
    pub fn is_host(&self) -> bool {
        match self {
            NetworkRole::Host => true,
            NetworkRole::Client => false,
        }
    }

    pub fn is_client(&self) -> bool {
        match self {
            NetworkRole::Host => false,
            NetworkRole::Client => true,
        }
    }
}


const PACKET_CLIENT_READY: u8 = 0x010;
const PACKET_PADDLE_DIRECTION_SYNC: u8 = 0x020;
const PACKET_PADDLE_HOST_POSITION_SYNC: u8 = 0x021;
const PACKET_PADDLE_CLIENT_POSITION_SYNC: u8 = 0x022;
const PACKET_BALL_SPAWN: u8 = 0x030;
const PACKET_BALL_SYNC: u8 = 0x031;
const PACKET_UPDATE_SCORE: u8 = 0x040;

#[derive(Debug)]
pub enum NetworkData {
    ClientReady,
    PaddleDirectionSync(i8),
    PaddleHostPositionSync(f32),
    PaddleClientPositionSync(f32),
    BallSpawn(f32),
    BallSync(f32, f32, f32, f32, BallColor),
    UpdateScore(u32, u32),
}

impl NetworkPacket for NetworkData {
    fn header(&self) -> u8 {
        match self {
            NetworkData::ClientReady => PACKET_CLIENT_READY,
            NetworkData::PaddleDirectionSync(..) => PACKET_PADDLE_DIRECTION_SYNC,
            NetworkData::PaddleHostPositionSync(..) => PACKET_PADDLE_HOST_POSITION_SYNC,
            NetworkData::PaddleClientPositionSync(..) => PACKET_PADDLE_CLIENT_POSITION_SYNC,
            NetworkData::BallSpawn(..) => PACKET_BALL_SPAWN,
            NetworkData::BallSync(..) => PACKET_BALL_SYNC,
            NetworkData::UpdateScore(..) => PACKET_UPDATE_SCORE,
        }
    }

    fn write_data(&self, stream: &mut impl WriteBytesExt) -> bool {
        match self {
            NetworkData::PaddleDirectionSync(direction) =>
                stream.write_i8(*direction).is_ok(),
            NetworkData::PaddleHostPositionSync(position) =>
                stream.write_f32::<NetworkEndian>(*position).is_ok(),
            NetworkData::PaddleClientPositionSync(position) =>
                stream.write_f32::<NetworkEndian>(*position).is_ok(),
            NetworkData::BallSpawn(angle) =>
                stream.write_f32::<NetworkEndian>(*angle).is_ok(),
            NetworkData::BallSync(x, y, vx, vy, c) => true
                && stream.write_f32::<NetworkEndian>(*x).is_ok()
                && stream.write_f32::<NetworkEndian>(*y).is_ok()
                && stream.write_f32::<NetworkEndian>(*vx).is_ok()
                && stream.write_f32::<NetworkEndian>(*vy).is_ok()
                && stream.write_u8((*c).into()).is_ok(),
            NetworkData::UpdateScore(host, client) => true
                && stream.write_u32::<NetworkEndian>(*host).is_ok()
                && stream.write_u32::<NetworkEndian>(*client).is_ok(),
            _ => true,
        }
    }

    fn read_data(header: u8, stream: &mut impl ReadBytesExt) -> Option<Self> {
        match header {
            PACKET_CLIENT_READY => NetworkData::ClientReady.into(),
            PACKET_PADDLE_DIRECTION_SYNC => NetworkData::PaddleDirectionSync(
                stream.read_i8().ok()?,
            ).into(),
            PACKET_PADDLE_HOST_POSITION_SYNC => NetworkData::PaddleHostPositionSync(
                stream.read_f32::<NetworkEndian>().ok()?,
            ).into(),
            PACKET_PADDLE_CLIENT_POSITION_SYNC => NetworkData::PaddleClientPositionSync(
                stream.read_f32::<NetworkEndian>().ok()?,
            ).into(),
            PACKET_BALL_SPAWN => NetworkData::BallSpawn(
                stream.read_f32::<NetworkEndian>().ok()?,
            ).into(),
            PACKET_BALL_SYNC => NetworkData::BallSync(
                stream.read_f32::<NetworkEndian>().ok()?,
                stream.read_f32::<NetworkEndian>().ok()?,
                stream.read_f32::<NetworkEndian>().ok()?,
                stream.read_f32::<NetworkEndian>().ok()?,
                stream.read_u8().ok()?.into(),
            ).into(),
            PACKET_UPDATE_SCORE => NetworkData::UpdateScore(
                stream.read_u32::<NetworkEndian>().ok()?,
                stream.read_u32::<NetworkEndian>().ok()?,
            ).into(),
            _ => {
                log::error("Network: Received invalid packet header!");
                None
            }
        }
    }
}
