use specs::prelude::*;

use crate::{
    core::time::DeltaTime,
    ecs::{Rotation, Scale},
};


pub enum AnimationKind {
    MenuButton,
    Drop,
}

pub enum PlayMode {
    Once,
    Loop,
}


#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Animation {
    duration: f32,
    timer: f32,
    kind: AnimationKind,
    play_mode: PlayMode,
}

impl Animation {
    pub fn hover_slow() -> Self {
        Self {
            duration: 5.0,
            timer: 0.0,
            kind: AnimationKind::MenuButton,
            play_mode: PlayMode::Loop,
        }
    }

    pub fn hover_fast() -> Self {
        Self {
            duration: 2.5,
            timer: 0.0,
            kind: AnimationKind::MenuButton,
            play_mode: PlayMode::Loop,
        }
    }

    pub fn drop() -> Self {
        Self {
            duration: 0.5,
            timer: 0.0,
            kind: AnimationKind::Drop,
            play_mode: PlayMode::Once,
        }
    }

    fn advance(&mut self, delta: f32) -> bool {
        self.timer += delta;
        match self.play_mode {
            PlayMode::Once => return self.timer < self.duration,
            PlayMode::Loop => self.timer %= self.duration,
        }
        true
    }

    fn factor(&self) -> f32 {
        f32::max(0.0, f32::min(1.0, self.timer / self.duration))
    }

    fn sin_factor(&self, scale: f32, phase: f32) -> f32 {
        f32::sin(self.factor() * 2.0 * std::f32::consts::PI * scale + phase.to_radians())
    }

    fn sin_factor_normalized(&self, scale: f32, phase: f32) -> f32 {
        (self.sin_factor(scale, phase - 90.0) + 1.0) / 2.0
    }
}


pub const ANIMATION_SYSTEM: &str = "animation";

pub struct AnimationSystem;

impl<'a> System<'a> for AnimationSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, DeltaTime>,
        WriteStorage<'a, Animation>,
        WriteStorage<'a, Rotation>,
        WriteStorage<'a, Scale>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (entities, delta, mut animations, mut rotations, mut scales) = data;
        for (entity, rotation, scale) in (&entities, &mut rotations, &mut scales).join() {
            if let Some(animation) = animations.get_mut(entity) {
                let done = !animation.advance(delta.as_f32());
                match animation.kind {
                    AnimationKind::MenuButton => {
                        let scale_factor = 1.0 + animation.sin_factor_normalized(2.0, 90.0) * 0.25;
                        *scale = Scale(scale_factor, scale_factor);
                        *rotation = Rotation(animation.sin_factor(1.0, 135.0) * -5.0);
                    }
                    AnimationKind::Drop => {
                        let factor = 1.0 - animation.factor();
                        let scl = 1.0 + factor * 10.0;
                        let rot = 90.0 * factor;
                        *scale = Scale(scl, scl);
                        *rotation = Rotation(rot);
                    }
                }

                if done {
                    animations.remove(entity);
                }
            }
        }
    }
}
