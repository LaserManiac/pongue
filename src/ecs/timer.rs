use specs::prelude::*;

use crate::core::{Clock, DeltaTime};


pub struct TimerSystem {
    clock: Clock,
}

impl TimerSystem {
    pub fn new() -> Self {
        Self {
            clock: Clock::new(),
        }
    }
}

impl<'a> System<'a> for TimerSystem {
    type SystemData = Write<'a, DeltaTime>;

    fn run(&mut self, mut delta: Self::SystemData) {
        *delta = self.clock.reset();
    }
}
