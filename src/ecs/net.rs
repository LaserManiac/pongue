use std::{fmt::Debug, sync::Arc};

use specs::prelude::*;

use shrev::EventChannel;

use crate::core::{log, NetworkEvent, TcpConnection, NetworkPacket, AsyncQueue};


pub struct NetworkSender<T: NetworkPacket>(AsyncQueue<T>);

impl<T: NetworkPacket> NetworkSender<T> {
    pub fn send(&self, data: T) {
        self.0.push(data);
    }
}


pub type NetworkReceiver<T> = EventChannel<NetworkEvent<T>>;


pub const NETWORK_SYSTEM: &str = "network";

pub struct NetworkSystem<T: NetworkPacket> {
    connection: Arc<TcpConnection<T>>,
}

impl<T: NetworkPacket> NetworkSystem<T> {
    pub fn new(connection: Arc<TcpConnection<T>>) -> Self {
        Self { connection }
    }
}

impl<'a, T: NetworkPacket + Sync + Debug> System<'a> for NetworkSystem<T> {
    type SystemData = WriteExpect<'a, NetworkReceiver<T>>;

    fn run(&mut self, mut receiver: Self::SystemData) {
        self.connection.receive(|event| {
            log::info(format!("Network: {:?}", event));
            receiver.single_write(event);
        });
    }

    fn setup(&mut self, res: &mut Resources) {
        res.insert(NetworkSender(self.connection.sender().clone()));
    }
}
