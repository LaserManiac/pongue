pub mod movement;
pub mod draw;
pub mod animation;
pub mod net;
pub mod input;
pub mod timer;


use specs::prelude::*;

use crate::graphics::Color;


pub use animation::{Animation, AnimationSystem, ANIMATION_SYSTEM};
pub use draw::{Sprite, Text, Origin, Rotation, Scale, Tint, Background, DrawSystem};
pub use input::{InputEvent, Key, InputSystem};
pub use movement::{Velocity, MovementSystem, MOVEMENT_SYSTEM};
pub use net::{NetworkSystem, NetworkSender, NetworkReceiver, NETWORK_SYSTEM};
pub use timer::TimerSystem;


#[derive(Component)]
#[storage(VecStorage)]
pub struct Position(pub f32, pub f32);


pub fn spawn_text(world: &World,
                  text: impl Into<String>,
                  x: f32,
                  y: f32,
                  color: Color,
                  scale: f32,
                  animation: impl Into<Option<Animation>>)
                  -> Entity {
    let mut builder = world.create_entity_unchecked()
        .with(Position(x, y))
        .with(Text(text.into()))
        .with(Tint(color))
        .with(Scale(scale, scale))
        .with(Rotation(0.0));

    if let Some(animation) = animation.into() {
        builder = builder.with(animation);
    }

    builder.build()
}
