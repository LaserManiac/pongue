use std::convert::TryFrom;

use glium::glutin::{EventsLoop, Event as GlutinEvent, WindowEvent, ElementState};

use specs::prelude::*;

use shrev::EventChannel;


pub use glium::glutin::VirtualKeyCode as Key;


#[derive(Copy, Clone, Debug)]
pub enum InputEvent {
    Close,
    Resize,
    Key(Key, bool),
    Char(char),
}

impl TryFrom<GlutinEvent> for InputEvent {
    type Error = ();

    fn try_from(event: GlutinEvent) -> Result<Self, Self::Error> {
        match event {
            GlutinEvent::WindowEvent { event, .. } => match event {
                WindowEvent::CloseRequested => Ok(InputEvent::Close),
                WindowEvent::Resized(_) => Ok(InputEvent::Resize),
                WindowEvent::ReceivedCharacter(value) => Ok(InputEvent::Char(value)),
                WindowEvent::KeyboardInput { input: press, .. } =>
                    press.virtual_keycode.map(|key| {
                        let pressed = match press.state {
                            ElementState::Pressed => true,
                            ElementState::Released => false,
                        };
                        InputEvent::Key(key, pressed)
                    }).ok_or(()),
                _ => Err(())
            }
            _ => Err(()),
        }
    }
}


pub struct InputSystem {
    events_loop: EventsLoop,
}

impl InputSystem {
    pub fn new(events_loop: EventsLoop) -> Self {
        Self { events_loop }
    }
}

impl<'a> System<'a> for InputSystem {
    type SystemData = Write<'a, EventChannel<InputEvent>>;

    fn run(&mut self, mut channel: Self::SystemData) {
        self.events_loop.poll_events(|event| {
            if let Ok(event) = InputEvent::try_from(event) {
                channel.single_write(event);
            }
        });
    }
}
