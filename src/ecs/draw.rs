use specs::prelude::*;

use shrev::{EventChannel, ReaderId};

use glium::Surface;

use crate::{
    math::Point2,
    ecs::{Position, input::InputEvent},
    core::{Window, WORLD_WIDTH, WORLD_HEIGHT},
    graphics::{Color, Camera, Font, TextureRegion, Viewport, TextAlign, Renderer},
};


#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Sprite(pub TextureRegion);

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Text(pub String);

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Tint(pub Color);

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Origin(pub f32, pub f32);

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Rotation(pub f32);

#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Scale(pub f32, pub f32);


pub struct Background {
    region: TextureRegion,
    color: Color,
}

impl Background {
    pub fn new(region: TextureRegion, color: Color) -> Self {
        Self { region, color }
    }
}


pub struct DrawSystem {
    input: Option<ReaderId<InputEvent>>,
    window: Window,
    renderer: Renderer,
}

impl DrawSystem {
    pub fn new(window: Window, renderer: Renderer) -> Self {
        Self { input: None, window, renderer }
    }
}

impl<'a> System<'a> for DrawSystem {
    type SystemData = (
        Entities<'a>,
        WriteExpect<'a, Viewport>,
        ReadExpect<'a, Camera>,
        ReadExpect<'a, EventChannel<InputEvent>>,
        ReadExpect<'a, Font>,
        ReadExpect<'a, Background>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Sprite>,
        ReadStorage<'a, Text>,
        ReadStorage<'a, Tint>,
        ReadStorage<'a, Origin>,
        ReadStorage<'a, Rotation>,
        ReadStorage<'a, Scale>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (
            entities,
            mut viewport,
            camera,
            input,
            font,
            background,
            positions,
            sprites,
            texts,
            tints,
            origins,
            rotations,
            scales
        ) = data;

        if let Some(reader) = &mut self.input {
            for event in input.read(reader) {
                if let InputEvent::Resize = event {
                    *viewport = self.window.compute_viewport(WORLD_WIDTH, WORLD_HEIGHT);
                }
            }
        }

        let mut frame = self.window.draw();
        frame.clear_color(0.01, 0.01, 0.01, 1.0);
        let mut batch = self.renderer.begin(frame, *camera.view_projection_matrix(), *viewport);

        let world_size = [WORLD_WIDTH, WORLD_HEIGHT];
        batch.region([0.0, 0.0], [0.0, 0.0], world_size, 0.0, background.region, background.color);

        for (entity, Position(x, y), Sprite(region)) in (&entities, &positions, &sprites).join() {
            let color = tints.get(entity)
                .map(|Tint(color)| *color)
                .unwrap_or(Color::white());
            let origin = origins.get(entity)
                .map(|Origin(x, y)| Point2::new(*x, *y))
                .unwrap_or(Point2::new(0.0, 0.0));
            let rotation = rotations.get(entity)
                .map(|Rotation(rotation)| *rotation)
                .unwrap_or(0.0);
            let scale = scales.get(entity)
                .map(|Scale(x, y)| [*x, *y])
                .unwrap_or([1.0, 1.0]);
            let position = [*x, *y];
            batch.region(position, origin, scale, rotation, *region, color);
        }

        for (entity, Position(x, y), Text(text)) in (&entities, &positions, &texts).join() {
            let color = tints.get(entity)
                .map(|Tint(color)| *color)
                .unwrap_or(Color::white());
            let rotation = rotations.get(entity)
                .map(|Rotation(rotation)| *rotation)
                .unwrap_or(0.0);
            let scale = scales.get(entity)
                .map(|Scale(x, y)| [*x, *y])
                .unwrap_or([1.0, 1.0]);
            let halign = TextAlign::Center;
            let valign = TextAlign::Center;
            let position = [*x, *y];
            batch.text(text, position, scale, rotation, color, &font, halign, valign);
        }

        batch.end();
    }

    fn setup(&mut self, res: &mut Resources) {
        self.input = Some(Write::<EventChannel<_>>::fetch(res).register_reader());
    }
}
