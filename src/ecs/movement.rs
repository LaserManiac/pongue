use specs::prelude::*;

use crate::{core::DeltaTime, ecs::Position};


#[derive(Component)]
#[storage(DenseVecStorage)]
pub struct Velocity(pub f32, pub f32);

impl Velocity {
    pub fn new_direction(dir: f32, vel: f32) -> Self {
        let x = dir.to_radians().cos() * vel;
        let y = dir.to_radians().sin() * vel;
        Self(x, y)
    }

    pub fn accelerate_towards(&self, target: f32, acceleration: f32) -> Self {
        let Velocity(x, y) = *self;
        let norm = f32::sqrt(x * x + y * y);
        let new_norm = f32::min(target, norm + acceleration);
        let factor = new_norm / norm;
        Velocity(x * factor, y * factor)
    }

    pub fn x(&self) -> f32 {
        self.0
    }

    pub fn y(&self) -> f32 {
        self.1
    }
}


pub const MOVEMENT_SYSTEM: &str = "movement";

pub struct MovementSystem;

impl<'a> System<'a> for MovementSystem {
    type SystemData = (
        ReadExpect<'a, DeltaTime>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, Velocity>,
    );

    fn run(&mut self, (delta, mut positions, velocities): Self::SystemData) {
        let delta = delta.as_f32();
        for (position, velocity) in (&mut positions, &velocities).join() {
            let dx = velocity.x() * delta;
            let dy = velocity.y() * delta;
            position.0 += dx;
            position.1 += dy;
        }
    }
}
