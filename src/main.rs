#[macro_use]
extern crate glium;
#[macro_use]
extern crate specs_derive;


mod core;
mod math;
mod graphics;
mod ecs;
mod game_logic;


use crate::{core::Game, game_logic::InitState};


fn main() {
    match Game::new::<InitState>(()) {
        Ok(game) => game.run(),
        Err(err) => {
            println!("Error: {}", err);
            let _ = std::io::stdin().read_line(&mut String::new());
        }
    }
}
