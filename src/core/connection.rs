use std::{
    fmt::Debug,
    net::{TcpListener, TcpStream, IpAddr},
    sync::{
        Arc,
        atomic::{AtomicBool, Ordering},
    },
};

use byteorder::{ReadBytesExt, WriteBytesExt};

use crate::core::{log, sync::AsyncQueue};


const PORT: u16 = 9001;


pub enum ReadPacket<T> {
    Packet(T),
    Continue,
    Error,
}


pub trait NetworkPacket: Send + Sized + Debug + 'static {
    fn header(&self) -> u8;

    fn write_data(&self, stream: &mut impl WriteBytesExt) -> bool;

    fn read_data(header: u8, stream: &mut impl ReadBytesExt) -> Option<Self>;

    fn read(stream: &mut TcpStream) -> ReadPacket<Self> {
        match stream.read_u8() {
            Ok(header) => {
                stream.set_nonblocking(false)
                    .expect("Failed to unset TCP stream NON-BLOCKING!");
                let result = match Self::read_data(header, stream) {
                    Some(packet) => ReadPacket::Packet(packet),
                    None => {
                        log::warn("Network: Failed to read packet!");
                        ReadPacket::Error
                    }
                };
                stream.set_nonblocking(true)
                    .expect("Failed to set TCP stream NON-BLOCKING!");
                result
            }
            Err(ref err) if err.kind() == std::io::ErrorKind::WouldBlock => ReadPacket::Continue,
            Err(err) => {
                log::warn(format!("Network: Error reading packet header: {:?}", err.kind()));
                ReadPacket::Error
            }
        }
    }

    fn write(&self, stream: &mut impl WriteBytesExt) -> bool {
        let result = stream.write_u8(self.header()).is_ok() && self.write_data(stream);
        if !result {
            log::warn("Network: Failed to write packet!");
        }
        result
    }
}


#[derive(Debug)]
pub enum NetworkEvent<T: NetworkPacket + Debug> {
    Error,
    Connected,
    Disconnected,
    Data(T),
}


pub struct TcpConnection<P: NetworkPacket> {
    sender: AsyncQueue<P>,
    receiver: AsyncQueue<NetworkEvent<P>>,
    stopper: Arc<AtomicBool>,
}

impl<P: NetworkPacket> TcpConnection<P> {
    pub fn host() -> Self {
        Self::run_thread(move |stopper| {
            let listener = TcpListener::bind(("0.0.0.0", PORT)).ok()?;
            listener.set_nonblocking(true).ok()?;
            loop {
                match listener.accept() {
                    Ok((stream, _)) => break Some(stream),
                    Err(ref err) if err.kind() == std::io::ErrorKind::WouldBlock
                        && !stopper.load(Ordering::Relaxed) => (),
                    _ => break None,
                }
            }
        })
    }

    pub fn connect(ip: IpAddr) -> Self {
        Self::run_thread(move |_| {
            TcpStream::connect((ip, PORT)).ok()
        })
    }

    pub fn sender(&self) -> &AsyncQueue<P> {
        &self.sender
    }

    pub fn receive<'a>(&self, f: impl FnMut(NetworkEvent<P>) + 'a) {
        self.receiver.drain(f)
    }

    pub fn stop(&self) {
        self.stopper.store(true, Ordering::Relaxed);
    }

    fn run_thread(f: impl FnOnce(Arc<AtomicBool>) -> Option<TcpStream> + Send + 'static) -> Self {
        let connection = Self {
            sender: AsyncQueue::new(),
            receiver: AsyncQueue::new(),
            stopper: Arc::new(AtomicBool::new(false)),
        };

        let mut sender = connection.sender.clone();
        let mut receiver = connection.receiver.clone();
        let stopper = connection.stopper.clone();

        std::thread::Builder::new()
            .name(String::from("network_thread"))
            .spawn(move || {
                match f(stopper.clone()) {
                    Some(mut stream) => {
                        if stream.set_nonblocking(true).is_err()
                            || stream.set_nodelay(true).is_err() {
                            receiver.push(NetworkEvent::Error);
                            return;
                        }

                        receiver.push(NetworkEvent::Connected);

                        while Self::send_impl(&mut stream, &mut sender)
                            && Self::receive_impl(&mut stream, &mut receiver) {
                            if stopper.load(Ordering::Relaxed) {
                                break;
                            }
                        }

                        receiver.push(NetworkEvent::Disconnected);
                        stopper.store(false, Ordering::Relaxed);
                    }
                    None => receiver.push(NetworkEvent::Error),
                };
            })
            .expect("Could not start network thread!");

        connection
    }

    fn send_impl(stream: &mut TcpStream, sender: &mut AsyncQueue<P>) -> bool {
        let mut no_errors = true;
        sender.drain(|packet| {
            if !packet.write(stream) {
                no_errors = false;
            }
        });
        no_errors
    }

    fn receive_impl(stream: &mut TcpStream, receiver: &mut AsyncQueue<NetworkEvent<P>>) -> bool {
        loop {
            let packet = match P::read(stream) {
                ReadPacket::Packet(packet) => packet,
                ReadPacket::Continue => break true,
                ReadPacket::Error => break false,
            };
            receiver.push(NetworkEvent::Data(packet));
        }
    }
}

impl<P: NetworkPacket> Clone for TcpConnection<P> {
    fn clone(&self) -> Self {
        Self {
            sender: self.sender.clone(),
            receiver: self.receiver.clone(),
            stopper: self.stopper.clone(),
        }
    }
}

impl<P: NetworkPacket> Drop for TcpConnection<P> {
    fn drop(&mut self) {
        self.stop();
    }
}
