use std::time::Instant;


pub struct Clock(Instant);

impl Clock {
    pub fn new() -> Self {
        Self(Instant::now())
    }

    pub fn reset(&mut self) -> DeltaTime {
        let now = Instant::now();
        let elapsed = (now - self.0).as_micros() as f64 / 1000000.0;
        self.0 = now;
        DeltaTime(elapsed)
    }
}


#[derive(Copy, Clone)]
pub struct DeltaTime(f64);

impl DeltaTime {
    pub fn as_f32(&self) -> f32 {
        self.0 as f32
    }
}

impl Default for DeltaTime {
    fn default() -> Self {
        Self(0.0)
    }
}
