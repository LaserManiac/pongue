#[inline]
#[cfg(feature = "log")]
pub fn info(message: impl AsRef<str>) {
    println!("[INFO] {}", message.as_ref());
}

#[cfg(not(feature = "log"))]
pub fn info(message: impl AsRef<str>) {}

#[inline]
#[cfg(feature = "log")]
pub fn warn(message: impl AsRef<str>) {
    println!("[WARN] {}", message.as_ref());
}

#[cfg(not(feature = "log"))]
pub fn warn(message: impl AsRef<str>) {}

#[inline]
#[cfg(feature = "log")]
pub fn error(message: impl AsRef<str>) {
    println!("[ERROR] {}", message.as_ref());
}

#[cfg(not(feature = "log"))]
pub fn error(message: impl AsRef<str>) {}
