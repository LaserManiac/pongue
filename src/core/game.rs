use specs::prelude::*;

use glium::glutin::EventsLoop;

use crate::{
    core::{log, Window},
    graphics::{Camera, Texture, Renderer},
    ecs::{Position, InputSystem, TimerSystem, MovementSystem, Animation, AnimationSystem, Sprite,
          Text, Tint, Origin, Rotation, Scale, DrawSystem, MOVEMENT_SYSTEM, ANIMATION_SYSTEM},
};


pub const WORLD_WIDTH: f32 = 72.0;
pub const WORLD_HEIGHT: f32 = 96.0;
pub const WORLD_MIDDLE: f32 = WORLD_WIDTH * 0.5;
pub const WORLD_CENTER: f32 = WORLD_HEIGHT * 0.5;
const RENDERER_BATCH_SIZE: usize = 1000;
const RES_SPRITE_SHEET: &str = "./res/spritesheet.png";


pub trait GameState {
    fn update(&mut self, world: &mut World) -> Command;

    fn end(self: Box<Self>, world: &mut World);

    fn name(&self) -> &'static str;
}


pub trait GameStateConstructor {
    type Data;
    fn construct(world: &mut World, data: Self::Data) -> Self;
}


pub type DynGameStateConstructor = dyn FnOnce(&mut World) -> Box<dyn GameState>;


pub enum Command {
    Continue,
    Exit,
    Switch(Box<DynGameStateConstructor>),
}

impl Command {
    pub fn to<T: GameState + GameStateConstructor + 'static>(data: T::Data) -> Command {
        Command::Switch(Box::new(move |world| Box::new(T::construct(world, data))))
    }
}


const TITLE: &str = "Pongue";
const WINDOW_INIT_WIDTH: u32 = 800;
const WINDOW_INIT_HEIGHT: u32 = 600;

pub struct Game {
    world: World,
    state: Option<Box<dyn GameState>>,
    dispatcher: Dispatcher<'static, 'static>,
}

impl Game {
    pub fn new<T>(data: T::Data) -> Result<Self, String>
        where T: GameState + GameStateConstructor + 'static {
        log::info("Initializing engine...");

        let events = EventsLoop::new();
        let window = Window::new(TITLE, WINDOW_INIT_WIDTH, WINDOW_INIT_HEIGHT, &events)
            .map_err(|e| format!("Could not open game window: {}", e))?;
        log::info("> Initialized window...");

        let spritesheet = Texture::from_file(RES_SPRITE_SHEET, &window.context())
            .map_err(|e| format!("Could not load sprite sheet: {}", e))?;
        let renderer = Renderer::new(&window.context(), RENDERER_BATCH_SIZE, spritesheet);
        log::info("> Initialized renderer...");

        let mut world = World::new();
        // Core ECS
        world.register::<Position>();
        // Renderer
        world.add_resource(Camera::new(0.0, 0.0, WORLD_WIDTH, WORLD_HEIGHT));
        world.add_resource(window.compute_viewport(WORLD_WIDTH, WORLD_HEIGHT));
        world.register::<Sprite>();
        world.register::<Text>();
        world.register::<Tint>();
        world.register::<Origin>();
        world.register::<Rotation>();
        world.register::<Scale>();
        // Animation
        world.register::<Animation>();
        log::info("> Initialized world...");

        let state = Box::new(T::construct(&mut world, data));
        log::info(format!("> Initialized state to {}...", state.name()));

        let mut dispatcher = DispatcherBuilder::new()
            .with(MovementSystem, MOVEMENT_SYSTEM, &[])
            .with(AnimationSystem, ANIMATION_SYSTEM, &[])
            .with_thread_local(InputSystem::new(events))
            .with_thread_local(DrawSystem::new(window, renderer))
            .with_thread_local(TimerSystem::new())
            .build();
        dispatcher.setup(&mut world.res);
        log::info("> Initialized core dispatcher...");

        log::info("Initialized engine!");
        Ok(Self { world, state: Some(state), dispatcher })
    }

    pub fn run(mut self) {
        log::info("Running engine...");

        loop {
            let state = self.state.as_mut().unwrap();
            match state.update(&mut self.world) {
                Command::Continue => (),
                Command::Exit => break,
                Command::Switch(state) => self.switch_state(state),
            }

            self.dispatcher.dispatch(&self.world.res);
            self.world.maintain();
        }

        self.state.unwrap().end(&mut self.world);
        log::info("Stopped engine!");
    }

    fn switch_state(&mut self, state: Box<DynGameStateConstructor>) {
        let old_state = self.state.take().unwrap();
        let old_state_name = old_state.name();
        old_state.end(&mut self.world);

        let new_state = state(&mut self.world);
        let new_state_name = new_state.name();
        self.state = Some(new_state);

        log::info(format!("State transition: {} -> {}", old_state_name, new_state_name));
    }
}
