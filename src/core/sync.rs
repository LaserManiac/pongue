use std::{
    collections::VecDeque,
    sync::{Arc, Mutex},
};


pub struct AsyncQueue<T: Send>(Arc<Mutex<VecDeque<T>>>);

impl<T: Send> Clone for AsyncQueue<T> {
    fn clone(&self) -> Self {
        Self(Arc::clone(&self.0))
    }
}

impl<T: Send> AsyncQueue<T> {
    pub fn new() -> Self {
        Self(Arc::new(Mutex::new(VecDeque::new())))
    }

    pub fn push(&self, item: T) {
        self.0.lock()
            .expect("Queue mutex was poisoned!")
            .push_front(item);
    }

    pub fn drain<'a>(&self, mut f: impl FnMut(T) + 'a) {
        let mut queue = self.0.lock()
            .expect("Queue mutex was poisoned!");
        while let Some(item) = queue.pop_back() {
            f(item);
        }
    }
}
