use std::rc::Rc;

use glium::{
    Display, Frame,
    backend::{Context, Facade},
    glutin::{EventsLoop, WindowBuilder, ContextBuilder},
};

use crate::graphics::renderer::Viewport;


pub struct Window {
    display: Display,
}

impl Window {
    pub fn new(title: &str,
               width: u32,
               height: u32,
               events_loop: &EventsLoop)
               -> Result<Self, String> {
        let window_builder = WindowBuilder::new()
            .with_title(title)
            .with_dimensions((width, height).into())
            .with_resizable(true);
        let context_builder = ContextBuilder::new()
            .with_vsync(false)
            .with_srgb(true);
        let display = Display::new(window_builder, context_builder, &events_loop)
            .map_err(|e| format!("Error creating display: {}", e))?;
        Ok(Self { display })
    }

    pub fn context(&self) -> &Rc<Context> {
        self.display.get_context()
    }

    pub fn draw(&self) -> Frame {
        self.display.draw()
    }

    pub fn compute_viewport(&self, world_width: f32, world_height: f32) -> Viewport {
        let (screen_width, screen_height) = self.display.get_framebuffer_dimensions();
        let screen_ratio = screen_width as f32 / screen_height as f32;
        let field_ratio = world_width / world_height;
        if screen_ratio >= field_ratio {
            let w = (screen_height as f32 * field_ratio) as u32;
            let h = screen_height;
            let x = (screen_width - w) / 2;
            let y = 0;
            Viewport::from([x, y, w, h])
        } else {
            let w = screen_width;
            let h = (screen_width as f32 / field_ratio) as u32;
            let x = 0;
            let y = (screen_height - h) / 2;
            Viewport::from([x, y, w, h])
        }
    }
}
