use std::{rc::Rc, ops::Deref};

use glium::{
    VertexBuffer, Surface, Program, DrawParameters, Blend, Frame, backend::Context,
    index::{NoIndices, PrimitiveType},
};

use crate::{
    math::{Point2, Vector2, Matrix4, Rect, Bounds},
    graphics::{Color, Font, Texture, TextureRegion},
};


const QUAD_VERTEX_COUNT: usize = 6;
const FONT_SPACING_H: f32 = 1.0;
const FONT_SPACING_V: f32 = 2.0;


#[allow(non_snake_case)]
#[derive(Copy, Clone)]
struct Vertex {
    pub a_Position: [f32; 2],
    pub a_Color: [u8; 4],
    pub a_TexCoord: [f32; 2],
}

implement_vertex!(
    Vertex,
    a_Position normalize(false),
    a_Color normalize(true),
    a_TexCoord normalize(false)
);


pub struct Renderer {
    batch_size: usize,
    cache: Vec<Vertex>,
    buffer: VertexBuffer<Vertex>,
    program: Program,
    texture: Texture,
}

impl Renderer {
    pub fn new(context: &Rc<Context>,
               batch_size: usize,
               texture: Texture)
               -> Self {
        let cache = Vec::with_capacity(batch_size);
        let buffer = VertexBuffer::empty(context, batch_size)
            .expect("Could not create vertex buffer for renderer!");
        let program = create_program(context);
        Self { batch_size, cache, buffer, program, texture }
    }

    pub fn begin(&mut self,
                 frame: Frame,
                 view_projection: Matrix4<f32>,
                 viewport: impl Into<Viewport>)
                 -> Batch {
        let surface = Some(frame);
        let renderer = self;
        let viewport = viewport.into();
        Batch { surface, renderer, view_projection, viewport }
    }
}


pub struct Batch<'a> {
    surface: Option<Frame>,
    renderer: &'a mut Renderer,
    view_projection: Matrix4<f32>,
    viewport: Viewport,
}

impl<'a> Batch<'a> {
    pub fn quad(&mut self,
                quad: impl Into<Rect<f32>>,
                origin: impl Into<Point2<f32>>,
                rotation: f32,
                uv: impl Into<Bounds<f32>>,
                color: Color) {
        self.ensure_capacity(QUAD_VERTEX_COUNT);

        let quad = quad.into();
        let uv = uv.into();
        let origin = origin.into();
        let rotation_origin = Point2::new(quad.x, quad.y);
        let origin_quad = Rect::new(quad.x - origin.x, quad.y - origin.y, quad.w, quad.h);

        let p0 = Point2::new(origin_quad.x, origin_quad.y)
            .rotate_around(rotation_origin, rotation);
        let p1 = Point2::new(origin_quad.x + origin_quad.w, origin_quad.y)
            .rotate_around(rotation_origin, rotation);
        let p2 = Point2::new(origin_quad.x + origin_quad.w, origin_quad.y + origin_quad.h)
            .rotate_around(rotation_origin, rotation);
        let p3 = Point2::new(origin_quad.x, origin_quad.y + origin_quad.h)
            .rotate_around(rotation_origin, rotation);

        let v0 = Vertex {
            a_Position: p0.into(),
            a_Color: color.into(),
            a_TexCoord: [uv.from.x, uv.from.y],
        };
        let v1 = Vertex {
            a_Position: p1.into(),
            a_Color: color.into(),
            a_TexCoord: [uv.to.x, uv.from.y],
        };
        let v2 = Vertex {
            a_Position: p2.into(),
            a_Color: color.into(),
            a_TexCoord: [uv.to.x, uv.to.y],
        };
        let v3 = Vertex {
            a_Position: p3.into(),
            a_Color: color.into(),
            a_TexCoord: [uv.from.x, uv.to.y],
        };

        self.renderer.cache.push(v0);
        self.renderer.cache.push(v2);
        self.renderer.cache.push(v1);

        self.renderer.cache.push(v0);
        self.renderer.cache.push(v3);
        self.renderer.cache.push(v2);
    }

    pub fn region(&mut self,
                  position: impl Into<Point2<f32>>,
                  origin: impl Into<Point2<f32>>,
                  scale: impl Into<Vector2<f32>>,
                  rotation: f32,
                  region: TextureRegion,
                  color: Color) {
        let uv = region.calculate_uv(&self.renderer.texture);
        let position = position.into();
        let scale = scale.into();
        let origin = origin.into();
        let origin = [origin.x * scale.x, origin.y * scale.y];
        let quad = [
            position.x,
            position.y,
            region.width() as f32 * scale.x,
            region.height() as f32 * scale.y,
        ];
        self.quad(quad, origin, rotation, uv, color);
    }

    pub fn text(&mut self,
                text: &str,
                position: impl Into<Point2<f32>>,
                scale: impl Into<Vector2<f32>>,
                rotation: f32,
                color: impl Into<Color>,
                font: &Font,
                halign: TextAlign,
                valign: TextAlign) {
        let position = position.into();
        let scale = scale.into();
        let color = color.into();
        let text_height = font.dimensions(text).1 as f32;
        let mut yoff = match valign {
            TextAlign::Start => 0.0,
            TextAlign::Center => -text_height / 2.0,
            TextAlign::End => -text_height,
        };
        let (glyph_width, glyph_height) = font.glyph_size();
        let step_x = glyph_width as f32 + FONT_SPACING_H;
        let step_y = glyph_height as f32 + FONT_SPACING_V;
        for l in text.lines() {
            let line_width = font.dimensions(l).0 as f32;
            let mut xoff = match halign {
                TextAlign::Start => 0.0,
                TextAlign::Center => -line_width / 2.0,
                TextAlign::End => -line_width,
            };
            for c in l.chars() {
                if c != ' ' {
                    if let Some(&region) = font.region(c) {
                        let origin = [-xoff, -yoff];
                        self.region(position, origin, scale, rotation, region, color);
                    }
                }
                xoff += step_x;
            }
            yoff += step_y;
        }
    }

    pub fn end(mut self) {
        self.flush();
        self.surface.take().unwrap().finish().expect("Rendering error!");
    }

    fn flush(&mut self) {
        let renderer = &mut self.renderer;
        let len = renderer.cache.len();
        if len > 0 {
            renderer.buffer.slice_mut(0..len)
                .unwrap()
                .write(&renderer.cache);
            let uniforms = uniform!(
                    u_ViewProjection: *self.view_projection.deref(),
                    u_Texture: renderer.texture.sampled(),
                );
            let params = DrawParameters {
                viewport: Some(glium::Rect {
                    left: self.viewport.x,
                    bottom: self.viewport.y,
                    width: self.viewport.w,
                    height: self.viewport.h,
                }),
                blend: Blend::alpha_blending(),
                ..Default::default()
            };
            self.surface.as_mut()
                .unwrap()
                .draw(renderer.buffer.slice(0..len).unwrap(),
                      NoIndices(PrimitiveType::TrianglesList),
                      &renderer.program,
                      &uniforms,
                      &params)
                .expect("Rendering error!");
            renderer.cache.clear();
        }
    }

    fn ensure_capacity(&mut self, capacity: usize) {
        if self.renderer.cache.len() + capacity > self.renderer.batch_size {
            self.flush();
        }
    }
}

impl<'a> Drop for Batch<'a> {
    fn drop(&mut self) {
        if let Some(_) = self.surface {
            panic!("Renderer batch dropped before Batch::end() was called!");
        }
    }
}


#[derive(Copy, Clone)]
pub struct Viewport(Rect<u32>);

impl Deref for Viewport {
    type Target = Rect<u32>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: Into<Rect<u32>>> From<T> for Viewport {
    fn from(viewport: T) -> Self {
        Self(viewport.into())
    }
}


#[derive(Copy, Clone)]
#[allow(unused)]
pub enum TextAlign {
    Start,
    Center,
    End,
}


fn create_program(context: &Rc<Context>) -> Program {
    let program = program!(
        context,
        330 => {
            vertex: r#"
                #version 330

                in vec2 a_Position;
                in vec4 a_Color;
                in vec2 a_TexCoord;

                uniform mat4 u_ViewProjection;

                out vec4 v_Color;
                out highp vec2 v_TexCoord;

                void main() {
                    v_Color = a_Color;
                    v_TexCoord = a_TexCoord;
                    gl_Position = u_ViewProjection * vec4(a_Position, 0.0, 1.0);
                }
            "#,
            fragment: r#"
                #version 330

                in vec4 v_Color;
                in vec2 v_TexCoord;

                uniform sampler2D u_Texture;

                out vec4 o_Color;

                void main() {
                    o_Color = v_Color * texture(u_Texture, v_TexCoord);
                }
            "#,
        }
    );
    program.expect("Error creating shader for renderer!")
}
