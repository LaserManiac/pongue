use std::{rc::Rc, path::Path};

use glium::{
    backend::Context,
    texture::{RawImage2d, SrgbTexture2d, SrgbFormat, MipmapsOption},
    uniforms::{Sampler, MinifySamplerFilter, MagnifySamplerFilter},
};

use crate::math::bounds::Bounds;


pub struct Texture(SrgbTexture2d);

impl Texture {
    pub fn from_file(path: impl AsRef<Path>, context: &Rc<Context>) -> Result<Self, String> {
        let data = std::fs::read(path.as_ref())
            .map_err(|e| format!("Could not open image file: {}", e))?;
        let image = image::load_from_memory(&data)
            .map_err(|e| format!("Could not load image: {}", e))?;
        let image = image.to_rgba();
        let dimensions = image.dimensions();
        let image = RawImage2d::from_raw_rgba(image.into_vec(), dimensions);
        let format = SrgbFormat::U8U8U8U8;
        let mipmap = MipmapsOption::NoMipmap;
        let texture = SrgbTexture2d::with_format(context, image, format, mipmap)
            .map_err(|e| format!("Could not create texture object: {}", e))?;
        Ok(Self(texture))
    }

    pub fn width(&self) -> u32 {
        self.0.dimensions().0
    }

    pub fn height(&self) -> u32 {
        self.0.dimensions().1
    }

    pub(super) fn sampled(&self) -> Sampler<SrgbTexture2d> {
        self.0.sampled()
            .minify_filter(MinifySamplerFilter::Nearest)
            .magnify_filter(MagnifySamplerFilter::Nearest)
    }
}


const UV_OFFSET: f32 = 0.005;


#[derive(Copy, Clone)]
pub struct TextureRegion(Bounds<u32>);

impl TextureRegion {
    pub fn new(x: u32, y: u32, w: u32, h: u32) -> Self {
        Self(Bounds::new([x, y], [x + w, y + h]))
    }

    pub fn width(&self) -> u32 {
        self.0.width()
    }

    pub fn height(&self) -> u32 {
        self.0.height()
    }

    pub fn calculate_uv(&self, texture: &Texture) -> Bounds<f32> {
        let Self(region) = self;
        let w = texture.width() as f32;
        let h = texture.height() as f32;
        Bounds::new(
            [region.from.x as f32 / w, region.from.y as f32 / h],
            [(region.to.x as f32 - UV_OFFSET) / w, (region.to.y as f32 - UV_OFFSET) / h],
        )
    }

    pub fn make_sub_region(&self, x: u32, y: u32, w: u32, h: u32) -> Self {
        let Self(region) = self;
        Self(Bounds::new(
            [region.from.x + x, region.from.y + y],
            [region.from.x + x + w, region.from.y + y + h],
        ))
    }
}
