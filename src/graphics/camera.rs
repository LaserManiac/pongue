use crate::math::Matrix4;


pub struct Camera(Matrix4<f32>);

impl Camera {
    pub fn new(x: f32, y: f32, w: f32, h: f32) -> Self {
        Self(Matrix4::orthographic(x, x + w, y + h, y, 0.0, 1000.0))
    }

    pub fn view_projection_matrix(&self) -> &Matrix4<f32> {
        &self.0
    }
}
