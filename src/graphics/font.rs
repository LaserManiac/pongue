use std::{sync::Arc, collections::HashMap};

use crate::{
    graphics::TextureRegion,
    math::{Vector2, Point2},
};


#[derive(Clone)]
pub struct Font(Arc<FontData>);

impl Font {
    pub fn new(glyphs: &str,
               glyph_size: impl Into<Vector2<u32>>,
               region: TextureRegion,
               default: Option<impl Into<Point2<u32>>>)
               -> Self {
        let data = FontData::new(glyphs, glyph_size, region, default);
        Self(Arc::new(data))
    }

    pub fn region(&self, glyph: char) -> Option<&TextureRegion> {
        self.0.region(glyph)
    }

    pub fn dimensions(&self, text: &str) -> (u32, u32) {
        self.0.dimensions(text)
    }

    pub fn glyph_size(&self) -> (u32, u32) {
        (self.0.glyph_size.x, self.0.glyph_size.y)
    }
}


struct FontData {
    glyph_map: HashMap<char, TextureRegion>,
    glyph_size: Vector2<u32>,
    default: Option<TextureRegion>,
}

impl FontData {
    pub fn new(glyphs: &str,
               glyph_size: impl Into<Vector2<u32>>,
               region: TextureRegion,
               default: Option<impl Into<Point2<u32>>>)
               -> Self {
        let glyph_size = glyph_size.into();

        let width_rem = region.width() % glyph_size.x;
        assert_eq!(width_rem, 0, "Region width must be a multiple of glyph width!");
        let height_rem = region.height() % glyph_size.y;
        assert_eq!(height_rem, 0, "Region height must be a multiple of glyph height!");

        let col_count = region.width() / glyph_size.x;
        let mut glyph_map = HashMap::new();
        for (i, c) in glyphs.chars().enumerate() {
            let x = (i as u32 % col_count) * glyph_size.x;
            let y = (i as u32 / col_count) * glyph_size.y;
            let region = region.make_sub_region(x, y, glyph_size.x, glyph_size.y);
            glyph_map.insert(c, region);
        }

        let default = default.map(|pos| {
            let pos = pos.into();
            region.make_sub_region(pos.x, pos.y, glyph_size.x, glyph_size.y)
        });

        Self { glyph_map, glyph_size, default }
    }

    pub fn region(&self, glyph: char) -> Option<&TextureRegion> {
        self.glyph_map.get(&glyph).or(self.default.as_ref())
    }

    pub fn dimensions(&self, text: &str) -> (u32, u32) {
        let mut w = 0;
        let mut h = 0;
        let mut cw = 0;
        for l in text.lines() {
            for c in l.chars() {
                if c == ' ' || self.region(c).is_some() {
                    if cw != 0 { cw += 1; }
                    cw += self.glyph_size.x;
                }
            }
            if h != 0 { h += 1; }
            h += self.glyph_size.y;
            w = w.max(cw);
            cw = 0;
        }
        (w, h)
    }
}
