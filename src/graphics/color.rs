#[derive(Copy, Clone)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Color {
    pub const fn rgb(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b, a: 255 }
    }

    pub const fn white() -> Self {
        Color::rgb(255, 255, 255)
    }

    pub const fn black() -> Self {
        Color::rgb(0, 0, 0)
    }

    pub const fn light_gray() -> Self {
        Color::rgb(191, 191, 191)
    }
}

impl From<[u8; 4]> for Color {
    fn from(color: [u8; 4]) -> Self {
        let [r, g, b, a] = color;
        Self { r, g, b, a }
    }
}

impl Into<[u8; 4]> for Color {
    fn into(self) -> [u8; 4] {
        [self.r, self.g, self.b, self.a]
    }
}

