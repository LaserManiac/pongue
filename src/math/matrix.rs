use std::ops::Deref;


pub struct Matrix4<T> {
    data: [[T; 4]; 4]
}

impl<T: Clone> Clone for Matrix4<T> {
    fn clone(&self) -> Self {
        Self { data: self.data.clone() }
    }
}

impl<T: Copy> Copy for Matrix4<T> {}

impl Matrix4<f32> {
    pub fn orthographic(left: f32, right: f32, top: f32, bottom: f32, near: f32, far: f32) -> Self {
        let w = right - left;
        let h = bottom - top;
        let d = far - near;
        Self {
            data: [
                [2.0 / w, 0.0, 0.0, 0.0],
                [0.0, 2.0 / h, 0.0, 0.0],
                [0.0, 0.0, -2.0 / d, 0.0],
                [-(right + left) / w, -(bottom + top) / h, -(far + near) / d, 1.0],
            ]
        }
    }
}

impl<T> Deref for Matrix4<T> {
    type Target = [[T; 4]; 4];

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}
