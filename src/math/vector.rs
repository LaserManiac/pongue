use super::point::Point2;


pub struct Vector2<T> {
    pub x: T,
    pub y: T,
}

impl<T: Clone> Clone for Vector2<T> {
    fn clone(&self) -> Self {
        Self {
            x: self.x.clone(),
            y: self.y.clone(),
        }
    }
}

impl<T: Copy> Copy for Vector2<T> {}

impl<T> From<[T; 2]> for Vector2<T> {
    fn from([x, y]: [T; 2]) -> Self {
        Self { x, y }
    }
}

impl<T> From<Point2<T>> for Vector2<T> {
    fn from(Point2 { x, y }: Point2<T>) -> Self {
        Self { x, y }
    }
}
