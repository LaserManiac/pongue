use std::ops::Sub;

use crate::math::Point2;


pub struct Bounds<T> {
    pub from: Point2<T>,
    pub to: Point2<T>,
}

impl<T> Bounds<T> {
    pub fn new(from: impl Into<Point2<T>>, to: impl Into<Point2<T>>) -> Self {
        Self {
            from: from.into(),
            to: to.into(),
        }
    }
}

impl<T: PartialOrd + Sub<Output=T> + Copy> Bounds<T> {
    pub fn width(&self) -> T {
        if self.from.x < self.to.x {
            self.to.x - self.from.x
        } else {
            self.from.x - self.to.x
        }
    }

    pub fn height(&self) -> T {
        if self.from.y < self.to.y {
            self.to.y - self.from.y
        } else {
            self.from.y - self.to.y
        }
    }
}

impl<T: Clone> Clone for Bounds<T> {
    fn clone(&self) -> Self {
        Self {
            from: self.from.clone(),
            to: self.to.clone(),
        }
    }
}

impl<T: Copy> Copy for Bounds<T> {}
