use std::ops::{Add, Sub, Mul};

use super::vector::Vector2;


pub struct Point2<T> {
    pub x: T,
    pub y: T,
}

impl<T> Point2<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl<T: Add<Output=T> + Sub<Output=T> + Mul<f32, Output=T> + Clone> Point2<T> {
    pub fn rotate(&self, angle: f32) -> Self {
        let angle = angle.to_radians();
        let sin = angle.sin();
        let cos = angle.cos();
        Self {
            x: self.x.clone() * cos - self.y.clone() * sin,
            y: self.y.clone() * cos + self.x.clone() * sin,
        }
    }

    pub fn rotate_around(&self, origin: impl Into<Self>, angle: f32) -> Self {
        let origin = origin.into();
        let offset = self.clone() - origin.clone();
        let rotated = Point2::from(offset).rotate(angle);
        origin + Vector2::from(rotated)
    }
}

impl<T: Clone> Clone for Point2<T> {
    fn clone(&self) -> Self {
        Self {
            x: self.x.clone(),
            y: self.y.clone(),
        }
    }
}

impl<T: Copy> Copy for Point2<T> {}

impl<T> From<[T; 2]> for Point2<T> {
    fn from([x, y]: [T; 2]) -> Self {
        Self { x, y }
    }
}

impl<T> From<Vector2<T>> for Point2<T> {
    fn from(Vector2 { x, y }: Vector2<T>) -> Self {
        Self { x, y }
    }
}

impl<T> Into<[T; 2]> for Point2<T> {
    fn into(self) -> [T; 2] {
        [self.x, self.y]
    }
}

impl<T: Sub> Sub for Point2<T> {
    type Output = Vector2<T::Output>;

    fn sub(self, rhs: Self) -> Self::Output {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl<T: Add> Add<Vector2<T>> for Point2<T> {
    type Output = Point2<T::Output>;

    fn add(self, rhs: Vector2<T>) -> Self::Output {
        Self::Output {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
