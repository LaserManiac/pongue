pub struct Rect<T> {
    pub x: T,
    pub y: T,
    pub w: T,
    pub h: T,
}

impl<T> Rect<T> {
    pub fn new(x: T, y: T, w: T, h: T) -> Self {
        Rect { x, y, w, h }
    }
}

impl<T: Clone> Clone for Rect<T> {
    fn clone(&self) -> Self {
        Self {
            x: self.x.clone(),
            y: self.y.clone(),
            w: self.w.clone(),
            h: self.h.clone(),
        }
    }
}

impl<T: Copy> Copy for Rect<T> {}

impl<T> From<[T; 4]> for Rect<T> {
    fn from(rect: [T; 4]) -> Self {
        let [x, y, w, h] = rect;
        Self { x, y, w, h }
    }
}
